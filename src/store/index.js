import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        careers: [
            {id: 1, name: "Ingeniernía Civil Informática", pm: "600.00", pf: "783.70", pl: "673.00"},
            {id: 2, name: "Ingeniería Civil Biomédica", pm: "600.00", pf: "783.70", pl: "673.00"},
            {id: 3, name: "Ingeniería Civil en Ambiente", pm: "600.00", pf: "783.70", pl: "673.00"},
            {id: 4, name: "Ingeniería Civil en Biotecnología", pm: "600.00", pf: "783.70", pl: "673.00"},
            {id: 5, name: "Ingeniería Civil en Electricidad", pm: "600.00", pf: "783.70", pl: "673.00"},
            {id: 6, name: "Ingeniería Civil en Geografía", pm: "600.00", pf: "783.70", pl: "673.00"},
            {id: 7, name: "Ingeniería Civil en Industria", pm: "600.00", pf: "783.70", pl: "673.00"},
            {id: 8, name: "Ingeniería Civil en Mecánica", pm: "600.00", pf: "783.70", pl: "673.00"},
            {id: 9, name: "Ingeniería Civil en Metalurgia", pm: "600.00", pf: "783.70", pl: "673.00"},
            {id: 10, name: "Ingeniería Civil en Minas", pm: "600.00", pf: "783.70", pl: "673.00"},
            {id: 11, name: "Ingeniería Civil en Obras Civiles", pm: "600.00", pf: "783.70", pl: "673.00"},
            {id: 12, name: "Ingeniería Civil en Química", pm: "600.00", pf: "783.70", pl: "673.00"},
            {id: 13, name: "Ingeniería Civil en Telemática", pm: "600.00", pf: "783.70", pl: "673.00"},
            {id: 14, name: "Ingeniería Civil Mecatrónica", pm: "600.00", pf: "783.70", pl: "673.00"},
            {id: 15, name: "Ingeniería de Alimentos", pm: "600.00", pf: "783.70", pl: "673.00"},
            {id: 16, name: "Ingeniería de Ejecución en Climatización", pm: "600.00", pf: "783.70", pl: "673.00"},
            {id: 17, name: "Ingeniería de Ejecución en Computación e Informática", pm: "600.00", pf: "783.70", pl: "673.00"},
            {id: 18, name: "Ingeniería de Ejecución en Electricidad", pm: "600.00", pf: "783.70", pl: "673.00"},
            {id: 19, name: "Ingeniería de Ejecución en Geomensura", pm: "600.00", pf: "783.70", pl: "673.00"},
            {id: 20, name: "Ingeniería de Ejecución en Industria", pm: "600.00", pf: "783.70", pl: "673.00"},
            {id: 21, name: "Ingeniería de Ejecución en Mecánica", pm: "600.00", pf: "783.70", pl: "673.00"},
            {id: 22, name: "Ingeniería de Ejecución en Metalurgia", pm: "600.00", pf: "783.70", pl: "673.00"},
            {id: 23, name: "Ingeniería de Ejecución en Química", pm: "600.00", pf: "783.70", pl: "673.00"},
            {id: 24, name: "Ingeniería Estadística", pm: "600.00", pf: "783.70", pl: "673.00"},
            {id: 25, name: "Ingeniería en Agronegocios", pm: "600.00", pf: "783.70", pl: "673.00"},
            {id: 26, name: "Ingeniería en Física", pm: "600.00", pf: "783.70", pl: "673.00"},
            {id: 27, name: "Ingeniería en Matemática", pm: "600.00", pf: "783.70", pl: "673.00"},
            {id: 28, name: "Enfermería", pm: "600.00", pf: "783.70", pl: "673.00"}
        ],
        careersSelected: '',
        currentUserLocal: null
    },

    mutations: {
        addCareer(state, newCareer){
            if(state.careersSelected == ''){
                state.careersSelected = newCareer;
            }
            else{
                state.careersSelected = state.careersSelected + ',' + newCareer;
            }
        },

        cleanCareerSelected(state){
            state.careersSelected = [];
        },

        logOutUser(state){
            state.currentUserLocal = {}; 
            localStorage.setItem('localCurrentUser', JSON.stringify({}));
        },

        setCurrentUser(state, newUser){
            state.currentUserLocal = newUser;
            localStorage.setItem('localCurrentUser', JSON.stringify(newUser));
        }
    },

    actions: {

    },

    modules: {

    }
})
